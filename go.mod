module review

go 1.20

require (
	github.com/go-chi/chi v1.5.4
	github.com/jmoiron/sqlx v1.3.5
	github.com/lib/pq v1.10.9
	github.com/rs/zerolog v1.30.0
	github.com/streadway/amqp v1.1.0
	google.golang.org/protobuf v1.31.0
)

require (
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	golang.org/x/sys v0.11.0 // indirect
)
