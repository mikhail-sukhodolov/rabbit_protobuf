package service

import (
	"fmt"
	"log"

	"github.com/streadway/amqp"
	"google.golang.org/protobuf/proto"
	orders "review/client/internal/proto"
)

const (
	queueName         = "order_queue"
	responseQueueName = "response_queue" // Новая очередь для ответов
)

func RabbitSender(order orders.Order) orders.OrderStatus {
	conn, err := amqp.Dial("amqp://guest:guest@msgbroker:5672/")
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		queueName,
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	// Новая очередь для ответов
	responseQ, err := ch.QueueDeclare(
		responseQueueName,
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		log.Fatalf("Failed to declare a response queue: %v", err)
	}

	orderBytes, err := proto.Marshal(&order)
	if err != nil {
		log.Fatalf("Failed to marshal order: %v", err)
	}

	// Отправляем заказ в очередь
	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "application/octet-stream",
			Body:        orderBytes,
			ReplyTo:     responseQ.Name, // Указываем очередь для ответов
		})
	if err != nil {
		log.Fatalf("Failed to publish a message: %v", err)
	}

	fmt.Println("Sent order: ", order.OrderId, order.ProductName, order.Quantity, order.Client)

	// Читаем ответное сообщение
	msgs, err := ch.Consume(
		responseQ.Name,
		"",
		true,  // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer for response queue: %v", err)
	}

	orderStatus := &orders.OrderStatus{
		OrderId: order.OrderId,
		Success: true,
		Message: "Order successfully placed",
	}
	for msg := range msgs {
		err := proto.Unmarshal(msg.Body, orderStatus)
		if err != nil {
			log.Printf("Failed to unmarshal order status: %v", err)
			continue
		}
		log.Printf("Received order status: %v\n", orderStatus)
		return *orderStatus
	}
	return orders.OrderStatus{}
}
