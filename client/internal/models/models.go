package models

// Order представляет собой структуру заказа
type Order struct {
	OrderID     int32   `protobuf:"varint,1,opt,name=order_id,proto3" json:"order_id,omitempty"`
	ProductName string  `protobuf:"bytes,2,opt,name=product_name,proto3" json:"product_name,omitempty"`
	Quantity    int32   `protobuf:"varint,3,opt,name=quantity,proto3" json:"quantity,omitempty"`
	TotalPrice  float32 `protobuf:"fixed32,4,opt,name=total_price,proto3" json:"total_price,omitempty"`
	Client      int32   `protobuf:"varint,5,opt,name=client,proto3" json:"client,omitempty"`
}

// OrderStatus представляет собой статус заказа
type OrderStatus struct {
	OrderID int32  `protobuf:"varint,1,opt,name=order_id,proto3" json:"order_id,omitempty"`
	Success bool   `protobuf:"varint,2,opt,name=success,proto3" json:"success,omitempty"`
	Message string `protobuf:"bytes,3,opt,name=message,proto3" json:"message,omitempty"`
}
