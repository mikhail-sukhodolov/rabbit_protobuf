package controller

import (
	"encoding/json"
	"net/http"
	orders "review/client/internal/proto"
	"review/client/internal/service"
)

func PlaceOrder(w http.ResponseWriter, r *http.Request) {
	var req orders.Order
	var resp orders.OrderStatus

	//декодируем запрос
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	// передаем структуру в слой сервиса
	resp = service.RabbitSender(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	// отправляем ответ
	err = json.NewEncoder(w).Encode(resp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
