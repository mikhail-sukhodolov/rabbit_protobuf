package controller

import (
	"github.com/go-chi/chi"
)

func InitRoutes() *chi.Mux {
	r := chi.NewRouter()

	r.Route("/order", func(r chi.Router) {
		r.Post("/", PlaceOrder)

	})
	return r
}
