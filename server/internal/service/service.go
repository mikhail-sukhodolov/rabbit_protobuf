package service

import (
	orders "review/server/internal/proto"
)

type Orderer interface {
	GetOrder(order orders.Order) (*orders.OrderStatus, error)
}
type Service struct {
	storage Orderer
}

func NewService(storage Orderer) *Service {
	return &Service{storage: storage}
}

func (s *Service) GetOrder(order orders.Order) (*orders.OrderStatus, error) {
	return s.storage.GetOrder(order) // Вызываем метод GetOrder из PostgresDB
}
