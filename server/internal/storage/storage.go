package storage

import (
	"database/sql"
	"errors"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
	orders "review/server/internal/proto"
)

type PostgresDB struct {
	*sqlx.DB
}

func (p *PostgresDB) GetOrder(order orders.Order) (*orders.OrderStatus, error) {
	var result orders.OrderStatus
	var availableQuantity int32
	err := p.Get(&availableQuantity, "SELECT quantity FROM stock WHERE product_name = $1", order.ProductName)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			result := orders.OrderStatus{
				OrderId: order.OrderId,
				Success: false,
				Message: "No such product",
			}
			return &result, nil
		}
		log.Println(err)
		return nil, err
	}

	if availableQuantity <= 0 {
		result = orders.OrderStatus{
			OrderId: order.OrderId,
			Success: false,
			Message: "Product out of stock",
		}
		p.addOrderStatus(order, result)
		return &result, nil
	}

	if availableQuantity < order.Quantity {
		result = orders.OrderStatus{
			OrderId: order.OrderId,
			Success: false,
			Message: "No such quantity",
		}
		p.addOrderStatus(order, result)
		return &result, nil
	}

	tx, err := p.Begin()
	if err != nil {
		// Обработка ошибки при начале транзакции
		log.Println(err)
		return nil, err
	}

	_, err = tx.Exec("UPDATE orders SET quantity = quantity - $1 WHERE product_name = $2", order.Quantity, order.ProductName)
	if err != nil {
		// Откат транзакции в случае ошибки
		tx.Rollback()
		log.Println(err)
		return nil, err
	}

	// Завершение транзакции
	if err := tx.Commit(); err != nil {
		// Обработка ошибки при коммите транзакции
		log.Println(err)
		return nil, err
	}

	result = orders.OrderStatus{
		OrderId: order.OrderId,
		Success: true,
		Message: "Order successfully processed",
	}
	p.addOrderStatus(order, result)
	return &result, nil
}

func (p *PostgresDB) addOrderStatus(order orders.Order, status orders.OrderStatus) {
	query := `INSERT INTO orders (order_id,	product_name,quantity,total_price, client, status) VALUES ($1, $2, $3, $4, $5, $6)`
	_, err := p.DB.Exec(query, order.OrderId, order.ProductName, order.Quantity, order.TotalPrice, order.Client, status.Message)
	if err != nil {
		log.Println(err)
	}
}
