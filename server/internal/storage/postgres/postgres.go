package postgres

import (
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
)

func NewPostgresDB() (*sqlx.DB, error) {
	connStr := "user=postgres password=root dbname=postgres host=postgres port=5432 sslmode=disable"

	db, err := sqlx.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	createTablesQuery := `
		CREATE TABLE IF NOT EXISTS stock (
			product_name TEXT,
			quantity INT

		);
    INSERT INTO stock (product_name, quantity)
    SELECT 'product', 10
    WHERE NOT EXISTS (SELECT 1 FROM stock LIMIT 1);

		CREATE TABLE IF NOT EXISTS orders (
			order_id SERIAL PRIMARY KEY,
			product_name TEXT,
			quantity INT,
			total_price FLOAT,
			client INT,
		    status TEXT
		);
	`
	_, err = db.Exec(createTablesQuery)
	if err != nil {
		log.Fatal(err)
	}

	return db, nil
}
