package rabbit

import (
	"github.com/streadway/amqp"
	"google.golang.org/protobuf/proto"
	"log"
	orders "review/server/internal/proto"
	"review/server/internal/service"
)

type RabbitServer struct {
	service service.Orderer
}

func NewRabbitServer(service service.Orderer) *RabbitServer {
	return &RabbitServer{service: service}
}

func (r *RabbitServer) Run() {
	// подключаемся
	conn, err := amqp.Dial("amqp://guest:guest@msgbroker:5672/") // Укажите правильные параметры подключения
	if err != nil {
		log.Fatalf("Failed to connect to RabbitMQ: %v", err)
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("Failed to open a channel: %v", err)
	}
	defer ch.Close()

	// создаем очередь
	q, err := ch.QueueDeclare(
		"order_queue", // Имя очереди
		false, false, false, false, nil,
	)
	if err != nil {
		log.Fatalf("Failed to declare a queue: %v", err)
	}

	// создаем получателя
	msgs, err := ch.Consume(
		q.Name, "", true, false, false, false, nil,
	)
	if err != nil {
		log.Fatalf("Failed to register a consumer: %v", err)
	}

	// ожидаем сообщение
	for msg := range msgs {
		order := &orders.Order{}
		err := proto.Unmarshal(msg.Body, order)
		if err != nil {
			log.Printf("Failed to unmarshal order: %v", err)
			continue
		}
		log.Println("Received order:", order)

		//подготавливаем ответ
		status, err := r.service.GetOrder(*order)
		if err != nil {
			log.Println("service error", err)
		}

		//status := &orders.OrderStatus{
		//	OrderId: order.OrderId,
		//	Success: true,
		//	Message: "Successfully",
		//}

		//декодируем ответ в протобаф
		statusBytes, err := proto.Marshal(status)
		if err != nil {
			log.Printf("Failed to marshal status: %v", err)
			continue
		}

		// публикуем ответ в очередь
		err = ch.Publish(
			"", msg.ReplyTo,
			false, false,
			amqp.Publishing{
				ContentType:   "application/octet-stream",
				CorrelationId: msg.CorrelationId,
				Body:          statusBytes,
			},
		)
		if err != nil {
			log.Printf("Failed to publish status: %v", err)
			continue
		}
		log.Println("Order status sent successfully")
	}
}
