package main

import (
	"context"
	"github.com/rs/zerolog/log"
	"net/http"
	"os"
	"os/signal"
	"review/server/internal/rabbit"
	"review/server/internal/service"
	"review/server/internal/storage"
	"review/server/internal/storage/postgres"
	"time"
)

func main() {

	// запускаем сервер
	port := ":8081"
	server := http.Server{
		Addr:           port,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	go func() {
		log.Info().Msgf("Server starting at port %v", port)
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatal().Msg("Server stopped")
		}
	}()

	//инициализация
	db, err := postgres.NewPostgresDB()
	if err != nil {
		log.Printf("Failed to initialize Postgres database:", err)
	}
	defer db.Close()

	// Инициализация слоя хранилища
	storage := &storage.PostgresDB{DB: db}

	// Инициализация сервиса
	serv := service.NewService(storage)

	//запускаем сервер ожидания сообщения
	rabbit := rabbit.NewRabbitServer(serv)
	rabbit.Run()

	// ожидание сигнала завершения основного сервера
	quit := make(chan os.Signal, 1)
	// регистрируем обработчик для сигнала os.Interrupt
	signal.Notify(quit, os.Interrupt)
	// блокируем выполнение программы, пока не будет получен сигнал os. Interrupt
	<-quit
	log.Info().Msg("Shutdown Server")

	// тайм-аут завершения
	// создаем контекст ctx с таймаутом в 5 секунд и функцию cancel, которая может быть использована для отмены контекста
	// Если операция не завершится в течение указанного времени, контекст будет отменен и все горутины, связанные с этим контекстом, будут завершены.
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal().Msgf("Server shutdown failed, %v", err)
	}
	defer func(server *http.Server) {
		err := server.Close()
		if err != nil {

		}
	}(&server)

	log.Info().Msg("Server exiting")
}
